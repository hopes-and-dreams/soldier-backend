module gitlab.com/hopes-and-dreams/soldier-backend

go 1.17

require (
	github.com/Masterminds/squirrel v1.5.2
	github.com/fergusstrange/embedded-postgres v1.12.0
	github.com/go-chi/chi v1.5.4
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.4
	gitlab.com/hopes-and-dreams/authentication v0.0.0-20220116082026-4c80d339d28c
	gitlab.com/hopes-and-dreams/h-image v0.0.0-20220208091039-6d9fb0e13ce4
	gitlab.com/hopes-and-dreams/logs v0.0.0-20220205073923-3c556df3e92c
	gitlab.com/hopes-and-dreams/tools v0.4.9
)

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible // indirect
	github.com/aws/aws-sdk-go v1.39.0 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/go-redis/redis/v8 v8.11.4 // indirect
	github.com/golang-jwt/jwt/v4 v4.2.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.15.1 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	github.com/yuin/gopher-lua v0.0.0-20210529063254-f4c35e4016d9 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20220204135822-1c1b9b1eba6a // indirect
)
