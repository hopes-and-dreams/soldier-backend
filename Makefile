restartAll: restart migrateUp run

start: up migrateUp run 

restart: down up

up:
	docker-compose -f ./deployments/docker-compose.yml up -d
	sleep 2

down:
	docker-compose -f ./deployments/docker-compose.yml down -v

run: build
	./backend

build:
	go build -o backend ./main.go

migrateUp:
	migrate -source file://./deployments/migrations \
	-database "postgres://hopes-and-dreams:1234@localhost:5430/soldier-pq-db?sslmode=disable" up


migrateDown:
	migrate -source file://./deployments/migrations \
	-database "postgres://hopes-and-dreams:1234@localhost:5430/soldier-pq-db?sslmode=disable" down

# sshdb:
# 	psql -h localhost -p 5430 -U hopes-and-dreams -W soldier-pq-db

# testlabs
.PHONY: up down restart migrateUp migrateDown build
