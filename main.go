package main

import (
	"gitlab.com/hopes-and-dreams/soldier-backend/missions"
	"gitlab.com/hopes-and-dreams/soldier-backend/profile"
	"gitlab.com/hopes-and-dreams/soldier-backend/repository"
	"gitlab.com/hopes-and-dreams/soldier-backend/server"
	"gitlab.com/hopes-and-dreams/tools/env"
)

func init() {
	env.ReadEnv()
}

func main() {

	repo := repository.NewRepo()
	m := missions.NewMission(repo.MissionStore)
	p := profile.NewProfile(repo.ProfileStore)
	server := server.NewServer(p, m)
	server.Run()
}
