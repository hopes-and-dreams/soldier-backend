package server

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	authMW "gitlab.com/hopes-and-dreams/authentication/middleware"
	"gitlab.com/hopes-and-dreams/logs"
	"gitlab.com/hopes-and-dreams/soldier-backend/missions"
	"gitlab.com/hopes-and-dreams/soldier-backend/profile"

	"gitlab.com/hopes-and-dreams/tools/ctools"
	"gitlab.com/hopes-and-dreams/tools/rtools/mw"
)

type Server struct {
	Router *chi.Mux
	P      *profile.Profile
	M      *missions.Missions
}

func NewServer(p *profile.Profile, m *missions.Missions) *Server {
	server := &Server{
		P: p,
		M: m,
	}
	server.initializeRoute()
	return server
}

func (s *Server) Run() {
	port := "8000"
	if err := chi.Walk(s.Router, ctools.WalkFunc()); err != nil {
		logs.FatalError(err, "Logging err:%s\n")
	}
	logs.Info("Listening on port: " + port)
	log.Fatal(http.ListenAndServe(":"+port, s.Router))
}

func (s *Server) initializeRoute() {
	r := chi.NewRouter()
	r.Use(middleware.GetHead)
	r.Use(middleware.CleanPath)
	r.Use(middleware.Logger)
	r.Use(middleware.RequestID)
	r.HandleFunc("/", serveHome)
	r.Route(
		"/profile", func(r chi.Router) {
			r.Use(mw.Cors)
			r.Get("/{profileID}", s.P.GetProfile)
			r.With(authMW.AuthChecker).Route("/", func(r chi.Router) {
				r.Post("/", s.P.CreateProfile)
				r.Put("/", s.P.UpdateProfile)
				r.Put("/image", s.P.UpdateProfilePicture)
			})
		},
	)
	r.Route(
		"/missions", func(r chi.Router) {
			r.Use(mw.Cors)
			r.Get("/", s.M.GetMissions)
			r.With(authMW.AuthChecker).Route("/", func(r chi.Router) {
				r.Get("/{missionID}", s.M.GetMission)
				r.Post("/add-mission", s.M.CreateMission)
			})
		},
	)
	s.Router = r
}

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	fmt.Fprintf(w, "Welcome to soldier backend home!")
}
