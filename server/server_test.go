package server

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/hopes-and-dreams/h-image/image/s3"
	"gitlab.com/hopes-and-dreams/h-image/repository"
	"gitlab.com/hopes-and-dreams/logs"
	"gitlab.com/hopes-and-dreams/tools/env"
	"gitlab.com/hopes-and-dreams/tools/ttool"
)

func TestMain(m *testing.T) { // TODO: change func color
		// Prepare
		env.ReadEnv("../dev.env")

		connectionString := fmt.Sprintf("postgres://test:test@localhost:%d/test?sslmode=disable", dbPort)
		tDB, err := ttool.StartWholeDBWithMigration(getDBConfig(), connectionString)
		if err != nil {
			logs.InfoError(err, "error while starting DB with migration")
			os.Exit(1)
		}
		testDB = tDB
		mS3 = &mockedS3{}
		imgRepo := &repository.Repositorer{
			DB: testDB.GetDB(),
		}
		s := s3.NewService(mS3, imgRepo)
		server = NewServer(s, imgRepo)
	
		// Action
		code := m.Run()
	
		os.Exit(code)
}
