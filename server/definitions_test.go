package server

import (
	"os"

	embeddedpostgres "github.com/fergusstrange/embedded-postgres"
)

func getDBConfig() embeddedpostgres.Config {
	return embeddedpostgres.
		DefaultConfig().
		Username("test").
		Password("test").
		Database("test").
		Port(uint32(5011)).
		Logger(os.Stdout).
		BinariesPath("./.testdb").
		DataPath("./.testdb/data").
		RuntimePath("./.testdb")
}