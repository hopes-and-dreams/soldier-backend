INSERT INTO images(image_id_pk,
                   image_caption, created_at)
VALUES ('ec78a8e9-d0eb-4e01-8690-e04585ff1bb6',
        'A picture caption', CURRENT_TIMESTAMP),
        ('426a11e1-ceeb-432c-90a3-fb40f947a09a',
        'A caption haha', CURRENT_TIMESTAMP);

INSERT INTO profiles(profile_id_pk,
                     image_ids,
                     email,
                     profile_name,
                     birthdate,
                     created_at,
                     phone_number,
                     "state",
                     city,
                     about_me)
VALUES ('ac983f99-b19a-4829-a68f-772f466ca37c',
        '{"ec78a8e9-d0eb-4e01-8690-e04585ff1bb6"}',
        'squintyeyes005@gmail.com',
        'admin',
        '01/05/1995',
        CURRENT_TIMESTAMP,
        '+601019192929',
        'Johor', 'Pasir Gudang',
        'Im a potato');
