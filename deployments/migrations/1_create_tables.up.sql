CREATE TABLE profiles
(
    profile_id_pk UUID PRIMARY KEY,
    image_ids     TEXT[],
    email         VARCHAR(320) NOT NULL UNIQUE,
    profile_name  VARCHAR(255),
    birthdate     TEXT,
    created_at    TIMESTAMP,
    phone_number  VARCHAR(20),
    "state"       VARCHAR(255),
    city          VARCHAR(255),
    about_me      VARCHAR(1000)
);

CREATE TABLE images
(
    image_id_pk   UUID PRIMARY KEY,
    image_caption TEXT,
    created_at    timestamp NOT NULL
);

CREATE TABLE missions
(
    mission_id_pk UUID PRIMARY KEY,
    image_ids     TEXT[],
    applicant_ids TEXT[],
    created_at    TIMESTAMP,
    status        SMALLINT,
    "state"       VARCHAR(255),
    city          VARCHAR(255),
    boss_id       TEXT,
    reward        TEXT,
    title         TEXT,
    description   TEXT
);


CREATE INDEX profile_email_idx on profiles(email);

-- Index name standard {tablename}_{columnname(s)}_{suffix}
-- more info https://stackoverflow.com/questions/4107915/postgresql-default-constraint-names/4108266#4108266

------- Missions tables -----------

CREATE INDEX missions_created_atx ON missions(created_at);

CREATE INDEX missions_statusx ON missions(status);

CREATE INDEX missions_statex ON missions("state");

CREATE INDEX missions_cityx ON missions(city);