FROM golang:1.17

WORKDIR /go/src/app

COPY . .

RUN git config --global url."https://golang:glpat-1ZiDSySfaBksyX6Vp2x1@gitlab.com".insteadOf "https://gitlab.com"

RUN go mod tidy

RUN make build

CMD ["./h-ws"]