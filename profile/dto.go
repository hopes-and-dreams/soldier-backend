package profile

type UploadProfileBody struct {
	ProfileName string `json:"name"`
	Email       string `json:"email"`
	Birthdate   string `json:"birthdate"`
	PhoneNumber string `json:"phone_number"`
	State       string `json:"state"`
	City        string `json:"city"`
	AboutMe     string `json:"about_me"`
}

type DtoBody struct {
	ProfileID       string   `json:"profile_id"`
	ProfileName     string   `json:"profile_name"`
	ProfilePictures []string `json:"profile_pictures"`
	Email           string   `json:"email"`
	Birthdate       string   `json:"birthdate"`
	CreatedAt       string   `json:"created_at"`
	City            string   `json:"city"`
	State           string   `json:"state"`
	PhoneNumber     string   `json:"phone_number"`
	AboutMe         string   `json:"about_me"`
}

type UploadProfilePictureDto struct {
	ImageID string `json:"image_id"`
	Caption string `json:"caption"`
}
