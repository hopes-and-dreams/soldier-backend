package profile

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/hopes-and-dreams/authentication/jwt"
	imgRepo "gitlab.com/hopes-and-dreams/h-image/repository"
	"gitlab.com/hopes-and-dreams/logs"
	"gitlab.com/hopes-and-dreams/soldier-backend/repository"
	"gitlab.com/hopes-and-dreams/tools/ctools"
	"gitlab.com/hopes-and-dreams/tools/rtools"
)

type Profile struct {
	r     repository.Profiler
	imgSvcURL string
}

func NewProfile(repo repository.Profiler) *Profile {
	url := os.Getenv("image_service_url")
	return &Profile{
		r: repo,
		imgSvcURL: url,
	}
}

// Think about if we really need CreateProfile endpoint
func (p *Profile) CreateProfile(w http.ResponseWriter, r *http.Request) {

	profileID, err := jwt.ExtractClaims("jti", r)
	logs.InternalServerError(err, "error extracting jti claim from jwt token", w)
	userEmail, err := jwt.ExtractClaims("sub", r)
	logs.InternalServerError(err, "error extracting sub claim from jwt token", w)
	profileUUID, err := ctools.StringToUUID(profileID.(string))
	logs.InternalServerError(err, "error converting uuid string to uuid type", w)
	newProfile := repository.Profile{
		ProfileIDPk: profileUUID,
		Email:       userEmail.(string),
	}
	err = p.r.CreateProfile(&newProfile)
	logs.InternalServerError(err, "error creating profile", w)
	if err != nil {
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (p *Profile) GetProfile(w http.ResponseWriter, r *http.Request) {
	var dtoBody DtoBody
	profileID := chi.URLParam(r, "profileID")
	if !ctools.IsValidUUID(profileID) {
		logs.UnprocessableEntity(
			errors.New("profileID is not provided"),
			"error getting profileID from url param", w,
		)
	}
	profileUUID, err := ctools.StringToUUID(profileID)
	if err != nil {
		logs.UnprocessableEntity(err, "failed to convert profile id from string to uuid", w)
	}
	pf, err := p.r.Profile(profileUUID)
	switch err {
	case sql.ErrNoRows:
		logs.NotFound(err, "profile of id "+profileID+" is not found", w)
		return
	case nil:
		dtoBody = DtoBody{
			ProfileID:       pf.ProfileIDPk.String(),
			ProfileName:     pf.ProfileName,
			ProfilePictures: pf.ImageIDs,
			Email:           pf.Email,
			Birthdate:       pf.BirthDate,
			CreatedAt:       pf.CreatedAt.String(),
			City:            pf.City,
			State:           pf.State,
			AboutMe:         pf.AboutMe,
			PhoneNumber:     pf.PhoneNumber,
		}
	default:
		logs.InternalServerError(err, "error getting profile from db of "+profileID, w)
	}
	err = rtools.ReturnJsonDto(dtoBody, http.StatusOK, w)
	logs.InternalServerError(err, "error while marshalling json object", w)
}

// UpdateProfilePicture will only update the profile picture
func (p *Profile) UpdateProfilePicture(w http.ResponseWriter, r *http.Request) {
	var imageDto UploadProfilePictureDto
	err := rtools.ReadRequestBody(r, &imageDto)
	logs.InternalServerError(
		err, "error reading WithCaption body in UpdateProfilePicture", w,
	)

	profileID, err := jwt.ExtractClaims("jti", r)
	logs.ForbiddenError(err, "error extracting claims cookie token", w)
	logs.Info("profile.UpdateProfilePicture from jwt sub profileID: " + profileID.(string) + "imageID: " + imageDto.ImageID)

	// DB side
	err = p.r.UpdateProfileImage(profileID.(string), imageDto.ImageID)
	logs.InternalServerError(err, "error update profile image at the db", w)
	now := time.Now()
	imageUUID, err := ctools.StringToUUID(imageDto.ImageID)
	img := imgRepo.Image{
		ImageID: imageUUID, Caption: imageDto.Caption, CreatedAt: now,
	}
	b, err := json.Marshal(img)
	imgReader := bytes.NewReader(b)
	err = p.r.WithTransaction(func(tx repository.Transaction) error {
		updateStr := fmt.Sprintf(
			"UPDATE %s SET %s = ARRAY_APPEND(%s, '%s') WHERE %s = '%s'",
			repository.SqlProfile,
			repository.SqlImageIDs,
			repository.SqlImageIDs,
			imageDto.ImageID,
			repository.SqlProfileIDPk,
			profileID,
		)
		_, err = tx.Exec(updateStr)
		if err != nil {
			return fmt.Errorf(
				"error during transaction, updating profile of %s failed due to error: %w",
				profileID, err)
		}

		resp, err := http.Post(p.s3URL, "application/json", imgReader)
		if err != nil {
			return fmt.Errorf("error posting metadata to h-image %w", err)
		}

		if resp.StatusCode >= 300 {
			return fmt.Errorf("status code of %d received from h-image when posting metadata",
				resp.StatusCode)
		}
		return nil
	})
	// err = .CreateImage(&img)
	logs.InternalServerError(err, "error Creating image at database", w)
	if err != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (p *Profile) UpdateProfile(w http.ResponseWriter, r *http.Request) {
	var dto UploadProfileBody
	err := rtools.ReadRequestBody(r, &dto)
	logs.InternalServerError(
		err, "error reading UploadProfileBody in UpdateProfile", w,
	)
	userId, err := jwt.ExtractClaims("jti", r)
	logs.ForbiddenError(err, "error extracting claim from client's jwt token", w)
	userIdStr := userId.(string)

	profileUserIdPk, err := ctools.StringToUUID(userIdStr)
	if err != nil {
		logs.InternalServerError(
			err, "error Converting uuid string of "+userIdStr, w,
		)
	}
	profile := &repository.Profile{
		ProfileIDPk: profileUserIdPk,
		ProfileName: dto.ProfileName,
		PhoneNumber: dto.PhoneNumber,
		State:       dto.State,
		City:        dto.City,
		BirthDate:   dto.Birthdate,
		AboutMe:     dto.AboutMe,
	}
	logs.Info("Profile will update with: ", profile)
	err = p.r.UpdateProfile(profile)
	if err != nil {
		return
	}
	w.WriteHeader(http.StatusOK)
}
