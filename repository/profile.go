package repository

import (
	"fmt"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/gofrs/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"gitlab.com/hopes-and-dreams/logs"
)

type ProfileStore struct {
	*sqlx.DB
}

type Profile struct {
	ProfileIDPk uuid.UUID      `db:"profile_id_pk"`
	ImageIDs    pq.StringArray `db:"image_ids"`
	Email       string         `db:"email"`
	ProfileName string         `db:"profile_name"`
	BirthDate   string         `db:"birthdate"`
	CreatedAt   time.Time      `db:"created_at"`
	PhoneNumber string         `db:"phone_number"`
	State       string         `db:"state"`
	City        string         `db:"city"`
	AboutMe     string         `db:"about_me"`
}

type Profiler interface {
	Profile(profileID string) (Profile, error)
	CreateProfile(m *Profile) error
	UpdateProfile(m *Profile) error
	UpdateProfileImage(profileID, imageUUID string) error
	WithTransaction(fn TxFn) error
	DeleteProfile(id uuid.UUID) error
}

const (
	SqlProfileIDPk = "profile_id_pk"
	SqlImageIDs    = "image_ids"
	SqlProfile     = "profiles"
	SqlBirthdate   = "birthdate"
	SqlEmail       = "email"
	SqlProfileName = "profile_name"
	SqlPhoneNumber = "phone_number"
	SqlState       = "state"
	SqlCity        = "city"
	SqlAboutMe     = "about_me"
)

// WithTransaction creates a new transaction and handles rollback/commit based on the
// error object returned by the `TxFn`
func (p *ProfileStore) WithTransaction(fn TxFn) (err error) {
	tx, err := p.Beginx()
	if err != nil {
		return
	}

	err = fn(tx)
	if err != nil {
		errRollback := tx.Rollback()
		if errRollback != nil {
			return fmt.Errorf("error rollingback transaction : %v", errRollback)
		}
		return fmt.Errorf("error executing function in transaction : %v", err)
	}

	// all is fine commit
	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("error committing transaction : %v", err)
	}
	return nil
}

func (s *ProfileStore) Profile(profileID string) (
	Profile, error,
) {
	var p Profile
	queryStr, args, err := sq.Select("*").From(SqlProfile).Where(
		SqlProfileIDPk+"=?", profileID,
	).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return Profile{}, fmt.Errorf(
			"error getting queryStr string for profile %s: %w", profileID, err,
		)
	}
	if err := s.DB.QueryRowx(queryStr, args...).
		Scan(
			&p.ProfileIDPk,
			&p.ImageIDs,
			&p.Email,
			&p.ProfileName,
			&p.BirthDate,
			&p.CreatedAt,
			&p.PhoneNumber,
			&p.State,
			&p.City,
			&p.AboutMe,
		); err != nil {
		return Profile{}, err
	}
	return p, nil
}

func (s *ProfileStore) ProfileByEmail(email string) (Profile, error) {
	var p Profile
	queryStr, args, err := sq.Select("*").From(SqlProfile).Where(
		SqlEmail+"=?", email,
	).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return Profile{}, fmt.Errorf(
			"error getting queryStr string for profile %s: %w", email, err,
		)
	}
	if err := s.DB.QueryRowx(queryStr, args...).
		Scan(
			&p.ProfileIDPk,
			&p.ImageIDs,
			&p.Email,
			&p.ProfileName,
			&p.BirthDate,
			&p.CreatedAt,
			&p.PhoneNumber,
			&p.State,
			&p.City,
			&p.AboutMe,
		); err != nil {
		return Profile{}, fmt.Errorf(
			"error getting profile with queryStr %s: %w", queryStr, err,
		)
	}
	return p, nil
}

func (s *ProfileStore) CreateProfile(profile *Profile) error {
	createStr, args, err := sq.Insert(SqlProfile).Columns(
		SqlProfileIDPk,
		SqlImageIDs,
		SqlEmail,
		SqlProfileName,
		SqlBirthdate,
		SqlCreatedAt,
		SqlPhoneNumber,
		SqlState,
		SqlCity,
		SqlAboutMe,
	).Values(
		profile.ProfileIDPk.String(),
		pq.Array([]string{}),
		profile.Email,
		"",
		"",
		time.Now(),
		"",
		"",
		"",
		"",
	).Suffix("RETURNING *").PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return fmt.Errorf(
			"error creating profile createStr when creating profile: %w", err,
		)
	}
	logs.Info("updateString and args: ", createStr)
	_, err = s.DB.Exec(createStr, args...)
	if err != nil {
		if logs.IsPqDuplicated(err) {
			return nil
		}
		return fmt.Errorf(
			"error creating profile of %s: %w", profile.ProfileIDPk, err,
		)
	}
	return nil
}

func (s *ProfileStore) UpdateProfile(profile *Profile) error {
	updateStr := fmt.Sprintf(
		`UPDATE %s SET 
		%s = '%s',
		%s = '%s',
		%s = '%s',
		%s = '%s',
		%s = '%s',
		%s = '%s'
		WHERE %s = '%s'`,
		SqlProfile,
		SqlProfileName, profile.ProfileName,
		SqlBirthdate, profile.BirthDate,
		SqlPhoneNumber, profile.PhoneNumber,
		SqlState, profile.State,
		SqlCity, profile.City,
		SqlAboutMe, profile.AboutMe,
		SqlProfileIDPk, profile.ProfileIDPk,
	)
	logs.Info("updateString and args: ", updateStr)
	if _, err := s.DB.Exec(updateStr); err != nil {
		return fmt.Errorf(
			"error updating profile of %s: %w", profile.ProfileIDPk, err,
		)
	}
	return nil
}

func (s *ProfileStore) UpdateProfileImage(profileID, imageUUID string) error {
	updateStr := fmt.Sprintf(
		"UPDATE %s SET %s = ARRAY_APPEND(%s, '%s') WHERE %s = '%s'",
		SqlProfile,
		SqlImageIDs,
		SqlImageIDs,
		imageUUID,
		SqlProfileIDPk,
		profileID,
	)
	if _, err := s.DB.Exec(updateStr); err != nil {
		return fmt.Errorf(
			"error updating profile of %s: %w", profileID, err,
		)
	}
	return nil
}

func (s *ProfileStore) DeleteProfile(profileID uuid.UUID) error {
	deleteStr, _, err := sq.Delete(SqlProfile).Where(
		SqlProfileIDPk, profileID,
	).ToSql()
	if err != nil {
		return fmt.Errorf(
			"error create profile deleteStr when deleting profile of %s: %w",
			profileID.String(), err,
		)
	}
	if _, err := s.DB.Exec(deleteStr); err != nil {
		return fmt.Errorf(
			"error deleting chat room of %s: %w", profileID.String(), err,
		)
	}
	return nil
}
