package repository

import (
	"fmt"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/gofrs/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

const (
	Hiring    = iota
	OnGoing   = iota
	Completed = iota
)

type MissionStore struct {
	*sqlx.DB
}

type Mission struct {
	MissionIDPk  uuid.UUID      `db:"mission_id_pk"`
	ImageIDs     pq.StringArray `db:"image_ids"`
	ApplicantIDs pq.StringArray `db:"applicant_ids"`
	CreatedAt    time.Time      `db:"created_at"`
	Status       int            `db:"status"`
	State        string         `db:"state"` // null if Remote
	City         string         `db:"city"`  // null if Remote
	BossID       string         `db:"boss_id"`
	Reward       string         `db:"reward"`
	Title        string         `db:"title"`
	Description  string         `db:"description"`
}

type Missioner interface {
	Mission(missionID uuid.UUID) (Mission, error)
	MissionsByCity(city string) ([]Mission, error)
	MissionsByState(state string) ([]Mission, error)
	CreateMission(m *Mission) error
	UpdateMission(m *Mission) error
	UpdateMissionStatus(missionID, status string) error 
	UpdateMissionReward(missionID, reward string) error
	DeleteMission(id uuid.UUID) error
}

const (
	SqlMissions       = "missions"
	SqlMissionIDPk    = "mission_id_pk"
	SqlApplicationIDs = "applicant_ids"
	SqlStatus         = "status"
	SqlBossID         = "boss_id"
	SqlReward         = "reward"
	SqlTitle          = "title"
	SqlDescription    = "description"
)

// WithTransaction creates a new transaction and handles rollback/commit based on the
// error object returned by the `TxFn`
func (m *MissionStore) WithTransaction(fn TxFn) (err error) {
	tx, err := m.Beginx()
	if err != nil {
		return
	}

	err = fn(tx)
	if err != nil {
		errRollback := tx.Rollback()
		if errRollback != nil {
			return fmt.Errorf("error rollingback transaction : %v", errRollback)
		}
		return fmt.Errorf("error executing function in transaction : %v", err)
	}

	// all is fine commit
	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("error committing transaction : %v", err)
	}
	return nil
}

func (mRepo *MissionStore) Mission(missionID uuid.UUID) (Mission, error) {
	var m Mission
	queryStr, args, err := sq.Select("*").From(SqlMissions).Where(
		SqlMissionIDPk+"=?", missionID,
	).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return Mission{}, fmt.Errorf(
			"error getting queryStr string for mission %s: %w", missionID, err,
		)
	}
	if err := mRepo.Get(&m, queryStr, args...); err != nil {
		return Mission{}, fmt.Errorf(
			"error getting mission with queryStr %s: %w", queryStr, err,
		)
	}
	return m, nil
}

func (mRepo *MissionStore) Missions() ([]Mission, error) {
	var missions []Mission
	queryStr, _, err := sq.Select("*").From(SqlMissions).ToSql()
	if err != nil {
		return nil, fmt.Errorf(
			"error getting queryStr string for missions %s: %w", queryStr, err,
		)
	}
	if err := mRepo.Select(&missions, queryStr); err != nil {
		return nil, fmt.Errorf(
			"error getting missions with queryStr %s: %w", queryStr, err,
		)
	}
	//err = json.Unmarshal(p, &missions)
	//if err != nil {
	//	return nil, fmt.Errorf("error unmarshaling byte into missions")
	//}
	return missions, nil
}

func (mRepo *MissionStore) MissionsByCity(city string) ([]Mission, error) {
	var missions []Mission
	queryStr, args, err := sq.Select("*").From(SqlMissions).Where(
		SqlCity+"=?", city,
	).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return nil, fmt.Errorf("error generating queryStr for Messages: %w", err)
	}
	// get the query string for the array of message UserIDPk
	if err := mRepo.Select(&missions, queryStr, args...); err != nil {
		return nil, fmt.Errorf("error getting missions array: %w", err)
	}
	return missions, nil
}

func (mRepo *MissionStore) MissionsByState(state string) ([]Mission, error) {
	var missions []Mission
	queryStr, args, err := sq.Select("*").From(SqlMissions).Where(
		SqlState+"=?", state,
	).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return nil, fmt.Errorf("error generating queryStr for Messages: %w", err)
	}
	// get the query string for the array of message UserIDPk
	if err := mRepo.DB.Select(&missions, queryStr, args...); err != nil {
		return nil, fmt.Errorf("error getting missions array: %w", err)
	}
	return missions, nil
}

func (mRepo *MissionStore) CreateMission(m *Mission) error {
	createStr, args, err := sq.Insert(SqlMissions).Columns(
		SqlMissionIDPk, SqlImageIDs, SqlCreatedAt,
		SqlStatus, SqlState, SqlCity,
		SqlBossID, SqlReward, SqlTitle, SqlDescription, SqlApplicationIDs,
	).Values(
		m.MissionIDPk.String(),
		m.ImageIDs,
		m.CreatedAt,
		m.Status,
		m.State,
		m.City,
		m.BossID,
		m.Reward,
		m.Title,
		m.Description,
		m.ApplicantIDs,
	).Suffix("RETURNING *").PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return fmt.Errorf(
			"error creating mission createStr: %w", err,
		)
	}
	if err := mRepo.DB.Get(m, createStr, args...); err != nil {
		return fmt.Errorf(
			"error creating mission with UserIDPk %s and string of %s: %w",
			m.MissionIDPk.String(), createStr, err,
		)
	}
	return nil
}

func (mRepo *MissionStore) UpdateMission(m *Mission) error {
	updateStr, _, err := sq.Update(SqlMissions).
		Set(SqlMissionIDPk, m.MissionIDPk).
		Set(SqlImageIDs, m.ImageIDs).
		Set(SqlCreatedAt, m.CreatedAt).
		Set(SqlStatus, m.Status).
		Set(SqlState, m.State).
		Set(SqlCity, m.City).
		Set(SqlBossID, m.BossID).
		Set(SqlReward, m.Reward).
		Set(SqlTitle, m.Title).
		Set(SqlDescription, m.Description).
		Suffix("RETURNING *").ToSql()
	if err != nil {
		return fmt.Errorf(
			"error creating mission updateStr when updating m of %s: %w",
			m.MissionIDPk, err,
		)
	}
	if err := mRepo.DB.Get(m, updateStr); err != nil {
		return fmt.Errorf(
			"error updating mission of %s: %w", m.MissionIDPk, err,
		)
	}
	return nil
}

func (mRepo *MissionStore) UpdateMissionStatus(missionID, status string) error {
	updateStatusStr, _, err := sq.Update(SqlMissions).
		Set(SqlMissionIDPk, missionID).
		Set(SqlStatus, status).
		Suffix("RETURNING *").ToSql()
	if err != nil {
		return fmt.Errorf(
			"error creating mission updateStatusStr when updating mission of %s: %w",
			missionID, err,
		)
	}
	if err := mRepo.DB.Get(missionID, updateStatusStr); err != nil {
		return fmt.Errorf(
			"error updating mission of %s: %w", missionID, err,
		)
	}
	return nil
}

func (mRepo *MissionStore) UpdateMissionReward(missionID, reward string) error {
	updateRewardStr, _, err := sq.Update(SqlMissions).
		Set(SqlMissionIDPk, missionID).
		Set(SqlReward, reward).
		Suffix("RETURNING *").ToSql()
	if err != nil {
		return fmt.Errorf(
			"error creating updateRewardStr when updating mission of %s: %w",
			missionID, err,
		)
	}
	if err := mRepo.DB.Get(missionID, updateRewardStr); err != nil {
		return fmt.Errorf(
			"error updating mission of %s: %w", missionID, err,
		)
	}
	return nil
}

func (mRepo *MissionStore) UpdateMissionApplicant(missionID, applicantID string) error {
	updateApplicantStr, _, err := sq.Update(SqlMissions).
		Set(SqlMissionIDPk, missionID).
		Set(SqlApplicationIDs, applicantID).
		Suffix("RETURNING *").ToSql()
	if err != nil {
		return fmt.Errorf(
			"error creating updateApplicatStr when updating mission %s: %w",
			missionID, err,
		)
	}
	if err := mRepo.DB.Get(missionID, updateApplicantStr); err != nil {
		return fmt.Errorf("error updating mission of %s: %w", missionID, err)
	}
	return nil
}

func (mRepo *MissionStore) DeleteMission(missionID uuid.UUID) error {
	deleteMissionStr, _, err := sq.Delete(SqlMissions).Where(
		SqlMissionIDPk, missionID,
	).ToSql()
	if err != nil {
		return fmt.Errorf(
			"error create deleteMissionStr of %s: %w",
			deleteMissionStr, err,
		)
	}
	if _, err := mRepo.DB.Exec(deleteMissionStr); err != nil {
		return fmt.Errorf("error deleting mission of %s: %w", missionID, err)
	}
	return nil
}
