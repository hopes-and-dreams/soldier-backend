package repository

import (
	"database/sql"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/hopes-and-dreams/logs"
)

const (
	SqlCreatedAt = "created_at"
)

func NewRepo() *Store {
	dbStr := os.Getenv("db_str")
	db, err := sqlx.Open("postgres", dbStr)
	if err != nil {
		logs.FatalError(err, "error starting db")
	}
	if err := db.Ping(); err != nil {
		logs.FatalError(err, "error pinging the db with dbStr: "+dbStr)
	}

	return &Store{
		ProfileStore: &ProfileStore{
			DB: db,
		},
		MissionStore: &MissionStore{
			DB: db,
		},
	}
}

type Store struct {
	*ProfileStore
	*MissionStore
}

type Transaction interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
	Preparex(query string) (*sqlx.Stmt, error)
	Queryx(query string, args ...interface{}) (*sqlx.Rows, error)
	QueryRowx(query string, args ...interface{}) *sqlx.Row
}

// A Txfn is a function that will be called with an initialized `Transaction` object
// that can be used for executing statements and queries against a database.
type TxFn func(Transaction) error
