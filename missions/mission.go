package missions

import (
	"net/http"
	"time"

	"github.com/gofrs/uuid"
	"github.com/lib/pq"
	"gitlab.com/hopes-and-dreams/h-image/image"
	"gitlab.com/hopes-and-dreams/logs"
	"gitlab.com/hopes-and-dreams/soldier-backend/repository"
	"gitlab.com/hopes-and-dreams/tools/rtools"
)

type Missioner interface {
	CreateMission(w http.ResponseWriter, r *http.Request)
	GetMissions(w http.ResponseWriter, r *http.Request)
	GetMission(w http.ResponseWriter, r *http.Request)
}

type Missions struct {
	r repository.Missioner
}

func NewMission(repo repository.Missioner) *Missions {
	return &Missions{
		r: repo,
	}
}

func (m *Missions) CreateMission(w http.ResponseWriter, r *http.Request) {
	var dto CreateMissionDtoBody
	err := rtools.ReadRequestBody(r, &dto)
	logs.InternalServerError(
		err, "error reading CreateMissionDtoBody in CreateMission", w,
	)

	missionID, err := uuid.NewV4()
	logs.InternalServerError(err, "error generating imageID", w)

	logs.Info("createMission insertDB dto", dto)
	mission := repository.Mission{
		MissionIDPk:  missionID,
		ImageIDs:     pq.StringArray(dto.ImageIDs),
		ApplicantIDs: pq.StringArray{},
		CreatedAt:    time.Now(),
		Status:       repository.Hiring,
		State:        dto.State,
		City:         dto.City,
		BossID:       dto.BossID,
		Reward:       dto.Reward,
		Title:        dto.Title,
		Description:  dto.Description,
	}
	m.r.CreateMission(&mission)
	logs.Info("Mission Added to DB: ", mission)
	err = rtools.ReturnJsonDto(http.StatusOK, missionID, w)
	logs.Error(err, "error returning missionID after created a mission")
}

func (m *Missions) GetMissions(w http.ResponseWriter, r *http.Request) {

	ms, err := r.Missions()
	if err != nil {
		logs.InternalServerError(
			err, "error getting all missions from db", w,
		)
		return
	}

	var dtoMissions ReadMissionDtoBodies
	missions, err := dtoMissions.GetList(ms)
	logs.InternalServerError(err, "error while getting mission list", w)
	err = rtools.ReturnJsonDto(http.StatusOK, missions, w)
	logs.InternalServerError(err, "error while marshalling json object", w)
}

func (m *Missions) GetMission(w http.ResponseWriter, r *http.Request) {
	cookies := r.Cookies()
	logs.Info(cookies)
	var mDTO ReadMissionDtoBody

	missionID := rtools.GetChiUrlParam(r, "missionID")
	uuid, err := uuid.FromString(missionID)
	logs.InternalServerError(err, "error converting missionID string to uuid", w)
	mission, err := m.r.Mission(uuid)
	logs.InternalServerError(err, "error loading mission into dto", w)
	mDTO.LoadBody(mission)

	err = rtools.ReturnJsonDto(http.StatusCreated, mDTO, w)
	logs.InternalServerError(err, "error returning dto to client", w)
}

func readImagesWithCaption(imageKeys []string) ([]img.WithCaption, error) {
	var images []image.WithCaption
	for _, imageKey := range imageKeys {
		imej, err := image.ReadImage(imageKey)
		if err != nil {
			return nil, err
		}
		imgDB, err := repo.Image(imageKey)
		if err != nil {
			return nil, err
		}
		i := img.WithCaption{
			Image: imej.Src, Caption: imgDB.ImageCaption,
		}
		images = append(images, i)
	}
	return images, nil
}
