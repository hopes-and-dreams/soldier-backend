package missions

import "gitlab.com/hopes-and-dreams/soldier-backend/repository"

//  mission_id: string
//  state: string
//  city
//  price: Price
//  title: string
//  status: number
//  applicants: number
//  created_at: number
//  description: string
//  photos?: Photo[] // base64

type ReadMissionDtoBody struct {
	MissionID       string   `json:"mission_id"`
	Title           string   `json:"title"`
	ApplicantNumber int      `json:"applicant_number"`
	Description     string   `json:"description"`
	Status          int      `json:"status"`
	State           string   `json:"state"`
	City            string   `json:"city"`
	BossID          string   `json:"boss_id"`
	Reward          string   `json:"reward"`
	CreatedAt       string   `json:"created_at"`
	ImageIDs        []string `json:"image_ids"`
}

type CreateMissionDtoBody struct {
	Title       string   `json:"title"`
	City        string   `json:"city"`
	State       string   `json:"state"`
	BossID      string   `json:"boss_id"`
	Reward      string   `json:"reward"`
	ImageIDs    []string `json:"image_ids"`
	Description string   `json:"description"`
}

type ReadMissionDtoBodies []ReadMissionDtoBody

func (dtoBodies *ReadMissionDtoBodies) GetList(missions []repository.Mission) (
	ReadMissionDtoBodies, error,
) {
	var dtoList []ReadMissionDtoBody
	for _, m := range missions {
		var dtoBody ReadMissionDtoBody
		dtoBody.LoadBody(m)
		dtoList = append(dtoList, dtoBody)
	}
	return dtoList, nil
}

func (dto *ReadMissionDtoBody) LoadBody(m repository.Mission) {
	dto.MissionID = m.MissionIDPk.String()
	dto.BossID = m.BossID
	dto.City = m.City
	dto.State = m.State
	dto.Reward = m.Reward
	dto.Status = m.Status
	dto.CreatedAt = m.CreatedAt.String()
	dto.Description = m.Description
	dto.ApplicantNumber = len(m.ApplicantIDs)
	dto.Title = m.Title
	dto.ImageIDs = []string(m.ImageIDs)
}
